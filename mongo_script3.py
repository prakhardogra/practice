__author__ = 'prakhardogra'

from pymongo import MongoClient

client = MongoClient()

db = client.test2

c = db.tweets

cursor = c.aggregate(
    [
        {
            "$match":{
                "user.followers_count":{"$gt":0}, "user.friends_count":{"$gt":0}
            }
        },{
        "$project":{
            "ratio":{
                "$divide":["$user.followers_count","$user.friends_count"]
            },"screen name":"$user.screen_name"}
    },{
        "$sort":{
            "ratio":-1
        }
    }])


for doc in cursor:
    print doc['screen name']