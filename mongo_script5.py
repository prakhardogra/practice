__author__ = 'prakhardogra'

from pymongo import MongoClient

client = MongoClient()

db = client.test2

c = db.tweets

cursor = c.aggregate(
    [
        {"$match":{"retweet_count":{"$ne":'null'}}},
        {"$unwind":"$entities.hashtags"},
        {"$group":{"_id":"$entities.hashtags.text","avg_retweet":{"$avg":"$retweet_count"}}}
    ]
)
for doc in cursor:
    print doc