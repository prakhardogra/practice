__author__ = 'prakhardogra'

import pprint
from pymongo import MongoClient

client = MongoClient()

db = client.test2

c = db.tweets

cursor = c.aggregate(
    [
        {"$unwind":"$entities.user_mentions"},
        {"$group":{"_id":"$user.screen_name","count":{"$sum":1}}},
        {"$sort":{"count":-1}}])
for doc in cursor:
    pprint.pprint(doc)
    break

'''
count = 0
for doc in cursor:
    print doc["_id"]
    count += 1
print count
'''