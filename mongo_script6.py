__author__ = 'prakhardogra'

from pymongo import MongoClient

client = MongoClient()

db = client.test2

c = db.tweets

cursor = c.aggregate([
    {"$unwind":"$entities.hashtags"},
    {"$group":{"_id":"$user.screen_name","unique_hashtags":{"$addToSet":"$entities.hashtags.text"}}},
    {"$sort":{"_id":1}}])

for doc in cursor:
    print doc