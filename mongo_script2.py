__author__ = 'prakhardogra'

import pprint
from pymongo import MongoClient

client = MongoClient()

db = client.test2

c = db.tweets

#pprint.pprint(c.find_one())

#print c.aggregate([{"$group":{"_id":"$source"}}])


cursor = c.aggregate([{"$unwind":"$entities.user_mentions"
        },{
            "$group":{"_id":"$user.screen_name","mset":{"$addToSet":"$entities.user_mentions.screen_name"}}
        },{
            "$unwind":"$mset"
        },{
            "$group":{"_id":"$_id","count":{"$sum":1}}
        },{
            "$sort":{"count":-1}
        },{
            "$limit":10}])

'''
cursor = c.aggregate(
    [
        {
            "$group":
                {
                    "_id":"$user.screen_name","count":
                    {
                        "$sum":1
                    }
                }
        },
        {
            "$sort":
                {
                    "count":-1
                }
        }
    ]
)
'''
for doc in cursor:
    print doc
#print list
#pprint.pprint(list)