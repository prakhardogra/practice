# -*- coding: utf-8 -*-

# Scrapy settings for project3 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'project3'

SPIDER_MODULES = ['project3.spiders']
NEWSPIDER_MODULE = 'project3.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'project3 (+http://www.yourdomain.com)'
