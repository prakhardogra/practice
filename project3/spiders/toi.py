__author__ = 'prakhardogra'
import scrapy
from scrapy.http import Request
from project3.items import Project3Item
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from w3lib.html import remove_tags

class toiSpider(scrapy.Spider):
    name = 'toi'
    allowed_domains = ["indiatimes.com"]
    start_urls = ["http://timesofindia.indiatimes.com/"]
    rules = (
        Rule(LinkExtractor(allow="indiatimes.com"), callback="parse_event", follow=True),
    )

    def parse(self, response):
        #title =""
        list_of_links = []
        list_of_links = response.xpath('//a/@href').extract()
        print list_of_links
        for single_link in list_of_links:
            #url = response.urljoin(single_link.extract())
            single_link = "http://timesofindia.indiatimes.com" + single_link
            yield Request(url = single_link, callback = self.parse_event)

    def parse_event(self, response):
        item = Project3Item()
        #print "\n\n\n"
        content_list = response.xpath('//h1[@class="heading1"]/text()').extract()
        final_content = ""
        for content in content_list:
            final_content += content
        item['title'] = final_content

        #print item['title']
        #print "\n"
        content_list= response.xpath('//div[@class="Normal"]/text()').extract()
        #above one gives a list
        final_content = ""
        for content in content_list:
            final_content += content
        final_content = final_content.replace("\n", "")
        final_content = final_content.replace('\"', '')
        item['article'] = final_content
        #item['article'] = content_list
        #item['title'] = "".join(item['title'])
        #print['article']
        #print item
        if item['title']!= "":
            if item['article'] != "":
                yield item

        #code below to recursively parse d whole website

        next_page_list = response.xpath('//a/@href').extract()
        for next_page in next_page_list:
            if next_page:
                url = "http://timesofindia.indiatimes.com" + next_page
                yield scrapy.Request(url, self.parse_event)